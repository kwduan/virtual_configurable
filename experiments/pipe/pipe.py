#!/usr/bin/python

__author__ = 'Kewei Duan'

import os, sys, time


def readInChunks(fileObj, chunkSize=2048):
    """
    Lazy function to read a file piece by piece.
    Default chunk size: 2kB.
    """
    while True:
        data = fileObj.read(chunkSize)
        if not data:
            break
        yield data

def writeResultFile(size, item_f):
    item = "%.3f" % item_f
    file = open("result.csv", "ab")
    if(size == '1g'):
        file.write(item+'\n')
    else:
        file.write(item+',')

print "The child will write text to a pipe and "
print "the parent will read the text written by child..."

start_time = time.time()

# file descriptors r, w for reading and writing
r, w = os.pipe()

processid = os.fork()
if processid:
    # This is the parent process
    # Closes file descriptor w
    os.close(w)
    r = os.fdopen(r)
    print "Parent reading"
    file = open("test_data.txt", "w")
    file.write(r.read())
    file.close()
    time.sleep(float(sys.argv[2]))
    elapsed_time = time.time() - start_time
    writeResultFile(sys.argv[1], elapsed_time)
    sys.exit(0)
else:
    # This is the child process
    os.close(r)
    w = os.fdopen(w, 'w')
    print "Child writing"
    filepath = './'+sys.argv[1]+'.txt'
    print filepath
    f = open(filepath)
    for chuck in readInChunks(f):
        w.write(chuck)
    w.close()
    #time.sleep(float(sys.argv[2]))
    print "Child closing"
    sys.exit(0)