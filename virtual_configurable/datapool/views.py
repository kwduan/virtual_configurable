__author__ = 'Kewei Duan'

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FileUploadParser
from . import configureSettings
from . import models, utils

import os
from django.http import StreamingHttpResponse
from django.core.servers.basehttp import FileWrapper
from django.core.exceptions import ObjectDoesNotExist

import json, requests


class DataItemResponse(APIView):
    parser_classes = (FileUploadParser,)

    def get(self, request, username, data_group_name, data_name, format=None):
        """
        To get the data file.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        if utils.has_data_group_permission(username, data_group_name, request.user):
            relative_path = username + '/' + data_group_name + '/' + data_name
            absolute_path = configureSettings.sandboxRoot + relative_path

            if models.DataItem.objects.filter(creatingUser=username, dataGroupName=data_group_name,
                                              dataName=data_name).exists():
                chunk_size = 8192
                response = StreamingHttpResponse(FileWrapper(open(absolute_path), chunk_size))
                response['Content-Length'] = os.path.getsize(absolute_path)
                response['Content-Disposition'] = "attachment; filename=%s" % data_name
                return response
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def post(self, request, username, data_group_name, data_name, format=None):
        """
        To post data file and create an URI for it.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        # Is the dataitem already existed
        if utils.has_data_group_permission(username, data_group_name, request.user) != True:
            user_unicode = str(request.user).decode('utf-8')
            if (username == user_unicode):
                # Is the datagroup already existed
                if not models.DataGroup.objects.filter(creatingUser=request.user,
                                                       dataGroupName=data_group_name).exists():
                    datagroup_temp = models.DataGroup(creatingUser=request.user, dataGroupName=data_group_name)
                    datagroup_temp.save()
            else:
                return Response(status=status.HTTP_403_FORBIDDEN)

        username_temp = username
        if models.DataItem.objects.filter(creatingUser=username_temp, dataGroupName=data_group_name,
                                          dataName=data_name).exists():
            return Response(status=status.HTTP_409_CONFLICT)
        else:
            # Is the datagroup already existed
            if not models.DataGroup.objects.filter(creatingUser=username_temp,
                                                   dataGroupName=data_group_name).exists():
                datagroup_temp = models.DataGroup(creatingUser=username_temp, dataGroupName=data_group_name)
                datagroup_temp.save()

            # Save the file
            relative_path = username_temp + '/' + data_group_name + '/'
            absolute_path = configureSettings.sandboxRoot + relative_path
            explicit_URI = request.GET.get('URI', '')
            if not explicit_URI:
                # mimetype_temp = request.content_type
                file_obj = request.FILES['file']
                if not os.path.exists(absolute_path):
                    os.makedirs(absolute_path)
                file_temp = open(absolute_path + data_name, 'wb')
                for chunk in file_obj.chunks():
                    file_temp.write(chunk)
                    file_temp.flush()
                file_temp.close()
            else:
                auth = utils.TokenAuth(request.META.get('Authorization'))
                request_temp = requests.get(explicit_URI, auth=auth, stream=True)
                if request_temp.status_code == status.HTTP_409_CONFLICT:
                    return Response(
                        data='The Authorization information you provided cannot used to access the external URI.',
                        status=status.HTTP_409_CONFLICT)
                if not os.path.exists(absolute_path):
                    os.makedirs(absolute_path)
                with open(absolute_path + data_name, 'wb') as file_temp:
                    for chunk in request_temp.iter_content(chunk_size=8192):
                        file_temp.write(chunk)
                        file_temp.flush()
                    file_temp.close()

            # Create the dataItem instance
            data_item_temp = models.DataItem(creatingUser=username_temp, dataGroupName=data_group_name,
                                             dataName=data_name)
            data_item_temp.save()

            return Response(status=status.HTTP_202_ACCEPTED)

    """
    def put(self, request, username, data_group_name, data_name, format=None):
        # To avoid user accidently update the file that may incur URI confusion.
        # If user wants do that, do it explicitly by deleting first.
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    """

    def delete(self, request, username, data_group_name, data_name, format=None):
        """
        To delete the data file.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        username_temp = username
        if utils.has_data_group_permission(username, data_group_name, request.user):
            if not models.DataItem.objects.filter(creatingUser=username_temp, dataGroupName=data_group_name,
                                                  dataName=data_name).exists():
                return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                models.DataItem.objects.filter(creatingUser=username_temp, dataGroupName=data_group_name,
                                               dataName=data_name).delete()
                relative_path = username_temp + '/' + data_group_name + '/' + data_name
                absolute_path = configureSettings.sandboxRoot + relative_path
                os.remove(absolute_path)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


class DataGroupResponse(APIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = {JSONParser, }
    # return permission and links.
    # Data must be organized in one datagroup with multiple copies even in local machine. -
    # This is decided by the nature that legacy codes need inputs in the working directory
    def get(self, request, username, data_group_name, format=None):
        """

        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        allDataItems = models.DataItem.objects.filter(creatingUser=username, dataGroupName=data_group_name)
        fullJson = dict()
        for dataItem in allDataItems:
            dataName = dataItem.dataName
            fullJson[dataName] = utils.generateURI_for_DataItem(username + '/' + data_group_name + '/' + dataName + '"')
        return Response(data=fullJson, status=status.HTTP_200_OK)

    # only operate on permission
    def post(self, request, username, data_group_name, isPublic, format=None):
        """
        To create the data group and URI. If it is not a public group, the permitted user names list can be posted in
        JSON.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
            - name: body
              paramType: body
              type: string
              required: false
              description: The JSON string for permitted user names.
        """
        try:
            models.DataGroup.objects.get(creatingUser=username, dataGroupName=data_group_name)
            return Response(status=status.HTTP_409_CONFLICT)
        except ObjectDoesNotExist:
            if isPublic:
                models.DataGroup(creatingUser=username, dataGroupName=data_group_name).save()
            else:
                new_user_name_list = request.data
                models.DataGroup(creatingUser=username, dataGroupName=data_group_name,
                                 authorizedGroup=json.dumps(new_user_name_list)).save()()
            return Response(status=status.HTTP_200_OK)

    # only operate on permission
    def put(self, request, username, data_group_name, isPublic, format=None):
        """
        Update the permission level and permitted usernames list.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
            - name: body
              paramType: body
              type: string
              required: false
              description: The JSON string for permitted user names.
        """
        # Is datagroup existed, if not, crete it.
        try:
            datagroup_temp = models.DataGroup.objects.get(creatingUser=username, dataGroupName=data_group_name)
            if isPublic:
                datagroup_temp.update(isPublicAccess=True)
            else:
                new_user_name_list = request.data
                existing_user_name_list = json.loads(
                    datagroup_temp.authorizedGroup)
                for item in new_user_name_list:
                    if new_user_name_list not in existing_user_name_list:
                        new_list = existing_user_name_list.append(item)
                        datagroup_temp.update(authorizedGroup=json.dumps(new_list))
            return Response(status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            if isPublic:
                models.DataGroup(creatingUser=username, dataGroupName=data_group_name).save()
            else:
                new_user_name_list = request.data
                models.DataGroup(creatingUser=username, dataGroupName=data_group_name,
                                 isPublicAccess=False, authorizedGroup=json.dumps(new_user_name_list)).save()()
            return Response(status=status.HTTP_200_OK)

    # delete itself and all the related items in dataitem
    def delete(self, request, username, data_group_name, format=None):
        """
        Delete the data group and all the data items in the data group.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        try:
            datagroup_temp = models.DataGroup.objects.get(creatingUser=username, dataGroupName=data_group_name)
            dataitems_list_temp = models.DataItem.object.filter(creatingUser=username, dataGroupName=data_group_name)
            for dataitem_temp in dataitems_list_temp:
                dataitem_temp.delete()
            datagroup_temp.delete()
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response


class DataGroupPermissionResponse(APIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = {JSONParser, }

    def get(self, request, username, data_group_name, format=None):
        """
        Get the permitted user list.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        try:
            data_item_tmp = models.DataGroup.objects.get(creatingUser=username,
                                                         dataGroupName=data_group_name).authorizedGroup
            return Response(data_item_tmp, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
