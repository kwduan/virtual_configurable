__author__ = 'Kewei Duan'

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FileUploadParser
from rest_framework.renderers import JSONRenderer
from . import utils, models, serializers, container_status, docker


class ContainerManipulation(APIView):
    #To-do: the functions to get/post/delete a container that can be accessed in the server.
    #Now, these functions are doen through ADMIN pages of Django.

    def get(self):
        return

    def post(self):
        return

    def delete(self):
        return

class ServiceManipulation(APIView):
    #To-do: the functions to deploy/delete a service as a container runnable service.
    #Now, these functions are doen through ADMIN pages of Django.

    parser_classes = (FileUploadParser,)

    def get(self, request, username, service_name, format=None):
        if utils.has_service_permission(username, service_name, request.user):
            if not models.Vm_service.objects.filter(createUser=username, serviceName=service_name).exists():
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                model = models.Vm_service.objects.get(createUser=username, serviceName=service_name)
                serializer = serializers.VM_serviceSerializer(model)
                return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def post(self, request, username, service_name, format=None):
        return

    def delete(self, request, username, service_name, format=None):
        return

class ServiceInvocation(APIView):
    renderer_classes = (JSONRenderer,)

    def get(self, request, creator_name, service_name, format=None):
        """
        To invoke an application service which will be executed in an appropriate Docker container.
        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """

        #Get service needed software/library names by service_name from configuration JSON file
        if utils.has_service_permission(creator_name, service_name, request.user):
            if not models.Vm_service.objects.filter(createUser=creator_name, serviceName=service_name).exists():
                return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                model = models.Vm_service.objects.get(createUser=creator_name, serviceName=service_name)
                serializer = serializers.VM_serviceSerializer(model)
                d_model = serializer.data
                l_neededSoftware = utils.unicode_to_list(d_model['neededSoftware'])
        #Get container by service needed software/libarary names
                tmp_image_model = utils.get_matching_image(l_neededSoftware)
                tmp_image_serializer = serializers.VM_imageSerializer(tmp_image_model)
                image_name = tmp_image_serializer.data['imageName']
        #Start the service by configuration file and assign & record corresponding ports
                try:
                    port_number = container_status.get_port()
                    print 'use port:' + port_number
                    json_create = utils.fill_port_image_name_in_template_json(port_number, image_name)
                    json_resp = docker.createContainer(json_create)
                    container_id = json_resp['Id']
                    docker.startContainer(container_id)
                    container_status.add_container_port(container_id, port_number)
                except:
                    container_status.append_port(port_number)
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        #Run it as in non-virtualized service by calling the localhost port
                datagroup_name = request.GET.get('dg_name', '')
                auth_str = request.META.get('HTTP_AUTHORIZATION') #This allows director to act as the user.
                auth = utils.TokenAuth(auth_str)
                tmp_resp = utils.invoke_container_service(port_number, creator_name, service_name, datagroup_name, auth)
        #Stop the service and delete it
                docker.deleteContainer(container_id)
                container_status.remove_contain_port(container_id)
                return Response(data=tmp_resp.content, status=tmp_resp.status_code)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)