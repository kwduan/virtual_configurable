=========================================
README for Virtual Configurable Framework
=========================================


1 Introduction
==============

Virtual Configurable(VC) Framework aims to support the wrapping and visualization of **legacy codes**, scripts and I/O data totally through web services interface. It is designed to construct the **Web services and workflows** based on **Resource Oriented Architecture** (REST web services) for universal access **without extra proprietary libraries support**. It includes three sub-systems, *Datapool*, *Service Manager* and *Virtualization Director*. 

In them, *Datapool* and *Service Manager* can be used without *Virtualization Director* to provide the wrapping function in a configurable manner.

1.1 Datapool
------------
*Datapool* sub-system aims to provide the distributed and asynchronous data staging for those legacy codes based Web services. Those services can be interconnected in a scientific workflow. It also serves as a data sandbox for application execution instance, a private/public data repository for each user based on the scalable authentication and authorization system (JWT). Datapool can support the execution of Application Services (legacy codes) managed by Service Manager, which resides with Datapool in the same OS.
*Datapool* app is in /virtual_configurable/datapool/

1.2 Service Manger
------------------
*Service Manager* sub-system includes two main functions: 1) the deployment and publication of legacy codes based application services; 2) the execution of application services in a sandbox with the support of local Datapool residing in the same OS. A corresponding legacy codes description language in the form of JSON is associated with Service Manager. It also enables the authentication and authorization to each of the application services.
*Service Manager* app is in /virtual_configurable/service_manager/

1.3 Virtualization Director
---------------------------
*Virtualization Director* aims to work with *Datapool* and *Service Manager* to provide the virtualization of data and application services based on *Docker*. *Virtualization Director* app is in /virtual_configurable/virtualization_director/

2 Installation
==============

2.1 Prerequiste
---------------

The whole system is written based on Django REST framework. Python 2.7 is needed for execution. The service can run on Linux.

The git repository has included the /env to provide the python virtual environment for the execution. It can be started by:

::

    $ source env/bin/activate


Another way is to installed all the dependencies for your local python environment. You can install them by:

::

    $ pip install -r VC_requriements.txt


2.2 Configuration
-----------------

There are two configureSettings.py files need to be filled. One is datapool/configureSettings.py. The other is service_manager/configureSettings.py.

Following is the example of Datapool one:

::

    sandboxRoot = '<The folder path that will be used to save all the sandboxes, data for the datapool service>'
    porNumber = '<The port you used to start the service, fox example, 8000>'
    appRelativePath = 'virtual_configurable/datapool/' #User does not need to change this. The default one works

The example of service_manager/configureSettings.py

::

    appRepoRoot = '<The folder path that will be used to save application and their corresponding sandboxes>'
    sandboxRoot = '<The same as the datapool sandboxRoot>'  
    porNumber = '<The port you used to start the service, fox example, 8000>'  
    local_commands = { 
        "<empty>": "",
        "<bash>": "bash",
    } 

For the local_command section, you can add the location of the local executables. For example, the key "<bash>" in this example, is the placeholder in the command string of the application description. The value is the location of the bash. In this case, bash is in the system variable PATH. Relative path is given

There is no specific configuration file for Virtualization Director. Its functions are built based on Datapool and Service Manager. However, it assumes Docker is correctly installed in the machine and the local unix socket of Docker is accessible.


2.3 Run
----------

Then execute the python script in virt_configurable sub-folder:

::

    $ python manager.py runserver runserver 0.0.0.0:8000

The port number 8000 can be changed to any port that is convenient to you. By running this, all three components: Datapool,
Service Manager and Virtualization Director are started.


3 Service Interface
===================
The description of service interfaces of Datapool, Service Manager and Virtualization Director can be found at "http://<root>/docs/" after you run the system.

4 Application Description
=========================
One essential step to wrap a command-line based application is describe it in a way that the VC Framework can understand. In general, this framework can deal with command-line application with the input types of:

a. stdin, 
b. values as command arguments, 
c. file paths as command arguements, 
d. relative path hard-coded in code. 

It also can deal with output types of: 

a. stdout, 
b. file paths as command arguements, 
c. relative path hard-coded in code. 

When error happens, stderr can also be collected from the published service. 

The JSON schema of application description can be found at /schemas/app_description_schema.json in the repository.

Notes:
======
1. This is an on-going project. If you have any suggestions or questions, please contact kw.duan@gmail.com. Thanks.