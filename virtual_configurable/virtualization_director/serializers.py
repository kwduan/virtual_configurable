__author__ = 'Kewei Duan'

from rest_framework import serializers
from . import models


class VM_imageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vm_image
        fields = (
            'imageName',
            'osName',
            'osVersion',
            'supportedSoftware')


"""
class VM_containerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vm_container
        fields = {
            'image',
            'created',
            'containerName',
            'vmPort',
            'hostPort',
            'sharable'
        }
"""


class VM_serviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vm_service
        field = {
            'id',
            'container',
            'created',
            'createUser',
            'serviceName',
            'neededSoftware'
        }
