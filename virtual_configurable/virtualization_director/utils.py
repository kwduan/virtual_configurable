__author__ = 'Kewei Duan'

import ast, json, os, requests
from . import models, serializers
from requests.auth import AuthBase


class TokenAuth(AuthBase):
    """
    Inherit from AuthBase to generate JWT based Authorization header field
    """
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r

def unicode_to_list(uc_str):
    """ Convert the unicode string returned by the model to python list

    :param uc_str: Unicode String
    :return l_names: List
    """
    l_names = list()
    for item in ast.literal_eval(uc_str):
        l_names.append(item.encode('ascii'))
    return l_names


def unicode_to_dict(uc_str):
    """ Convert the unicode string returned by the model to python dictionary

    :param uc_str: Univode String
    :return d_temp: Dict
    """
    d_temp = ast.literal_eval(uc_str)
    return d_temp


def has_service_permission(username, service_name, actual_user):
    if models.Vm_service.objects.filter(createUser=username, serviceName=service_name).exists():
        """
        obj = models.Service.objects.get(createUser=username, serviceName=service_name)

        if obj.isPublicAccess:
            return True
        else:
            serializer = serializers.ServiceSerializer(obj)
            u_names = serializer.data.get('authorizedGroup')
            l_names = unicode_to_list(u_names)
            if str(actual_user) in l_names:
                return True
            else:
                return False
        """
        return True
    else:
        return False


def fill_port_image_name_in_template_json(port_number, image_name):
    """ fill the port field in the create template json file.

    :param port_number: The port number as string
    :param image_name: The imange name as string
    :return: the filled json dictionary
    """
    base_path = os.path.dirname(os.path.abspath(__file__))
    file_path = base_path + '/JSON_templates/create_template.json'
    with open(file_path) as data_file:
        data = json.load(data_file)

    data["HostConfig"]["PortBindings"]["8000/tcp"][0]["HostPort"] = port_number
    data["Image"] = image_name
    str_data = json.dumps(data)
    return str_data


def get_matching_image(l_neededsoftware):
    queryset = models.Vm_image.objects.all()
    serializer = serializers.VM_imageSerializer(queryset, many=True)
    for item in serializer.data:
        l_supportedsoftware = unicode_to_list(item['supportedSoftware'])
        if set(l_neededsoftware).issubset(l_supportedsoftware):
            return item
        else:
            continue
    return None


def invoke_container_service(port_number, owner_name, service_name, dg_name, auth):
    url = "http://localhost:" + port_number + "/" + "app_service" + "/" + owner_name + "/" + service_name + "?dg_name="\
          + dg_name
    r = requests.get(url, auth=auth)
    return r
