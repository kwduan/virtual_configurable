__author__ = 'Kewei Duan'

from rest_framework import serializers
from . import models


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Service
        fields = (
        'createUser', 'serviceName', 'commandString', 'inputNameList', 'outputNameList', 'inCommandArgumentInputList',
        'workingDirectoryInputNameList', 'workingDirectoryOutputNameList', 'isPublicAccess', 'isStdin',
        'isStdout',
        'authorizedGroup')
