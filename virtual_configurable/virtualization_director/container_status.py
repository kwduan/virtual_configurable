__author__ = 'me1kd'

port_stack = ['51000', '51001', '51002', '51003', '51004', '51005', '51006', '51007', '51008', '51009']
# User can modify this part to list the available ports for container to use.
# This also can be used for limiting container concurrent number. By default, there are 10 at most.

container_port_dict = {}

def get_port():
    """ Get port number from port stack. Return False if ports are used up.

    :return:
    """
    if len(port_stack)>0:
        return port_stack.pop()
    else:
        return False

def append_port(port):
    """ Append port number when it is not needed before used.

    :return:
    """
    port_stack.append(port)

def get_container_port(container_ID):
    """ Get port number of specific container identified by container's ID

    :param container_ID: String
    :return: container's port number in String
    """
    return container_port_dict[container_ID]

def add_container_port(container_ID, port):
    """ Add new container ID and port pair

    :param container_ID: String
    :param port: String
    :return:
    """
    container_port_dict[container_ID] = port
    return

def remove_contain_port(container_ID):
    """ Remove container ID and port pair by container's ID

    :param container_ID: String
    :return:
    """
    port_stack.append(container_port_dict[container_ID])
    del container_port_dict[container_ID]
    return