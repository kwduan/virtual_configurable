__author__ = 'Kewei Duan'

from django.db import models
import service_manager.models

class Vm_image(models.Model):
    id = models.AutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    imageName = models.CharField(max_length=50, blank=False)
    osName = models.CharField(max_length=50, blank=False)
    osVersion = models.CharField(max_length=50, blank=False)
    supportedSoftware = models.TextField(blank=False, default='[]') #The dictionary contains the pair of name and version.


    def __str__(self):
        return self.imageName

"""
# container is dynamic resource, the information lifecycle should be kept in memory rather than DB
class Vm_container(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.ForeignKey('Vm_image')
    created = models.DateTimeField(auto_now_add=True)
    containerName = models.CharField(max_length=50, blank=False)
    vmPort = models.IntegerField()
    hostPort = models.IntegerField(blank=True)
    sharable = models.BooleanField(default=False, blank=False)


    def __str__(self):
        return self.containerName
"""

class Vm_service(models.Model):
    id = models.AutoField(primary_key=True)
    #default_image = models.ForeignKey('VM_image', blank=True) #If there is a default one, use it.
    created = models.DateTimeField(auto_now_add=True)
    createUser = models.CharField(max_length=50, blank=False)
    serviceName = models.CharField(max_length=50, blank=False)
    neededSoftware = models.TextField(blank=False, default='[]')
    #service = models.ForeignKey('service_manager.models.Service')


    def __str__(self):
        return self.serviceName

