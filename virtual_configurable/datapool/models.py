__author__ = 'Kewei Duan'

from django.db import models


class DataItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    creatingUser = models.CharField(max_length=50, blank=False)
    dataGroupName = models.CharField(max_length=50, blank=False)
    dataName = models.CharField(max_length=50, blank=False)
    # fileType = models.CharField(max_length=30, blank=False, default='application/octet-stream')

    def __str__(self):
        return self.dataGroupName + '_' + self.dataName

    class Meta:
        unique_together = (('creatingUser', 'dataGroupName', 'dataName'),)


class DataGroup(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    creatingUser = models.CharField(max_length=50, blank=False)
    dataGroupName = models.CharField(max_length=50, blank=False)
    isPublicAccess = models.BooleanField(default=True)
    authorizedGroup = models.TextField(blank=False,
                                       default='[]')  # A list to store the ID of the user/users that can access this data group

    class Meta:
        unique_together = (('creatingUser', 'dataGroupName'),)
