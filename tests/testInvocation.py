__author__ = 'Kewei Duan'

import requests, unittest, os
from requests.auth import HTTPBasicAuth, AuthBase
from requests_jwt import JWTAuth


class TokenAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r


class TestDeploymentAndInvocation(unittest.TestCase):
    def Input1(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/input1'
        fn1 = os.path.join(os.path.dirname(__file__), 'test_inputs/testinput1')
        files = {'file': open(fn1, 'rb')}
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.post(url, files=files, auth=auth)
        # print(r)
        return r.status_code

    def Input2(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/input2'
        fn2 = os.path.join(os.path.dirname(__file__), 'test_inputs/testinput2')
        files = {'file': open(fn2, 'rb')}
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.post(url, files=files, auth=auth)
        # print(r)
        return r.status_code

    def ServicePost(self):
        url = 'http://127.0.0.1:8000/app_service/manage/me1kd/test_service'
        fn1 = os.path.join(os.path.dirname(__file__), 'test_inputs/test.sh.zip')
        fn2 = os.path.join(os.path.dirname(__file__), 'test_inputs/testconf.json')
        files = {'file': open(fn1, 'rb'),
                 'service_conf': open(fn2, 'rb')}
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.post(url, files=files, auth=auth)
        # print(r)
        return r.status_code

    def Invoke(self):
        url = 'http://127.0.0.1:8000/app_service/me1kd/test_service?dg_name=testg'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.get(url, auth=auth)
        # for chunk in r.iter_content(chunk_size=8192):
        #    print(chunk)
        # print(r)
        return r.status_code

    def ServiceDelete(self):
        url = 'http://127.0.0.1:8000/app_service/manage/me1kd/test_service'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.delete(url, auth=auth)
        # print(r)
        return r.status_code

    def Input1Delete(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/input1'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.delete(url, auth=auth)
        return r.status_code

    def Input2Delete(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/input2'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.delete(url, auth=auth)
        return r.status_code

    def test_deployment_workflow(self):
        self.assertEqual(self.Input1(), 202)
        self.assertEqual(self.Input2(), 202)
        self.assertEqual(self.ServicePost(), 201)
        self.assertEqual(self.Invoke(), 200)
        self.assertEqual(self.ServiceDelete(), 204)
        self.assertEqual(self.Input1Delete(), 204)
        self.assertEqual(self.Input2Delete(), 204)


suite = unittest.TestLoader().loadTestsFromTestCase(TestDeploymentAndInvocation)
unittest.TextTestRunner(verbosity=2).run(suite)
