__author__ = 'Kewei Duan'

import socket, json, ast
from . import configureSettings, models
from requests.auth import AuthBase


class TokenAuth(AuthBase):
    """
    Inherit from AuthBase to generate JWT based Authorization header field
    """
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r

def has_data_group_permission(username, data_group_name, actual_user):
    """ check if the current user has the permission to access this data item.

    :param username:
    :param data_group_name:
    :param actual_user:
    :return:
    """
    if models.DataGroup.objects.filter(creatingUser=username, dataGroupName=data_group_name).exists():
        obj = models.DataGroup.objects.get(creatingUser=username, dataGroupName=data_group_name)
        if obj.isPublicAccess:
            return True
        else:
            u_names = obj.authorizedGroup
            l_names = list()
            for item in ast.literal_eval(u_names):
                l_names.append(item.encode('ascii'))
            if str(actual_user) in l_names:
                return True
            else:
                return False
    else:
        return False

def generateURI_for_DataItem(local_address):
    ipaddress = socket.gethostbyname(socket.gethostname())
    fullURI = ipaddress + ':' + configureSettings.porNumber + '/' + configureSettings.appRelativePath + local_address
    return fullURI

def unicode_to_list(uc_str):
    """ Convert the unicode string returned by the model to python list

    :param uc_str: Unicode String
    :return l_names: List
    """
    l_names = list()
    for item in ast.literal_eval(uc_str):
        l_names.append(item.encode('ascii'))
    return l_names
