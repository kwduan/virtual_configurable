__author__ = 'Kewei Duan'

appRepoRoot = '/home/me1kd/docker_shared/appRepo/' #This needs to be changed to the path of your folder for applications.
sandboxRoot = '/home/me1kd/docker_shared/sandbox/' #This needs to be changed to the path of your sandbox for data.
porNumber = '8000'
local_commands = {
    "<empty>": "",
    "<bash>": "bash",
}  # you can add the location of the local executables.
# For example, the key "<bash>" in this example, is the placeholder in the command string of the application description.
# The value is the location of the bash.
# In this case, bash is in the system variable PATH. Relative path is given
