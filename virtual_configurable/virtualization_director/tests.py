from django.test import TestCase
from virtualization_director import utils

class UtilTestCase(TestCase):
    def fill_the_port(self):
        return utils.fill_port_in_template_json("50000")

    def test_utils(self):
        json_file = self.fill_the_port()
        #print json_file
        self.assertEqual(json_file["HostConfig"]["PortBindings"]["8000/tcp"][0]["HostPort"], "50000")

