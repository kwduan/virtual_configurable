__author__ = 'Kewei Duan'

import requests_unixsocket, json

# To run this, Docker needs to be installed as prerequiste.

local_unix_socket_path = 'http+unix://%2Fvar%2Frun%2Fdocker.sock'

def testGetList():
    session = requests_unixsocket.Session()
    url = 'http+unix://%2Fvar%2Frun%2Fdocker.sock/containers/json'
    r = session.get(url, stream=True)
    print(r)
    for chunk in r.iter_content(chunk_size=8192):
        print(chunk)

def createContainer(json_definition):
    #print json_definition
    session = requests_unixsocket.Session()
    url = local_unix_socket_path+'/containers/create'
    #json_string  = json.dumps(json_definition)
    headers = {'Content-type': 'application/json'}
    r = session.post(url, data=json_definition, headers=headers)
    #resp_info = json.loads(r.text)
    return r

def testCreateContainer():
    file = open('API_test_inputs/create_template.json','r')
    create_json = file.read()
    r=createContainer(create_json)
    print r
    print r.content


def inspectContainer():
    session = requests_unixsocket.Session()
    url = local_unix_socket_path+'/containers/1ddb5a2b36b2/json'
    r = session.get(url, stream=True)
    print(r)
    for chunk in r.iter_content(chunk_size=8192):
        print(chunk)


#testGetList()
testCreateContainer()
#inspectContainer()
