__author__ = 'me1kd'

import os, shutil

test_list = ['100m', '200m', '300m', '400m', '500m', '600m', '700m', '800m', '900m', '1g']

test_data_folder = '/home/me1kd/Develop/Projects/Test_Apps/test_data/'

for num in range(0, 9):
    print 'start loop ' + str(num)
    for test_item in test_list:
	print 'start test' + test_item
        shutil.copyfile(test_data_folder+test_item+'.txt', './'+test_item+'.txt')
        os.system('python pipe.py '+test_item+' 0')
        os.remove('./'+test_item+'.txt')
        os.remove('./test_data.txt')