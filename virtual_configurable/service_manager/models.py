__author__ = 'Kewei Duan'

from django.db import models

class Service(models.Model):
    id = models.AutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    createUser = models.CharField(max_length=50, blank=False)
    serviceName = models.CharField(max_length=50, blank=False)
    commandString = models.CharField(max_length=200, blank=False)
    inputNameList = models.TextField(blank=False, default='[]')
    outputNameList = models.TextField(blank=False, default='[]')
    inCommandArgumentInputList = models.TextField(blank=False, default='[]')
    workingDirectoryInputNameList = models.TextField(blank=False, default='{}')
    workingDirectoryOutputNameList = models.TextField(blank=False, default='{}')
    isStdin = models.BooleanField(default=False)
    isStdout = models.BooleanField(default=False)
    isPublicAccess = models.BooleanField(default=True)
    authorizedGroup = models.TextField(blank=False,default='[]')  # A list to store the ID of the user/users that can access this data group

    def __str__(self):
        return self.serviceName

    class Meta:
        unique_together = (('createUser', 'serviceName'),)
