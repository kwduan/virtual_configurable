__author__ = 'me1kd'

from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
                       url(r'^(?P<creator_name>\w+)/(?P<service_name>\w+)$', views.ServiceInvocation.as_view()),
                       )
