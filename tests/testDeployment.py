__author__ = 'Kewei Duan'

import requests, unittest, os
from requests.auth import HTTPBasicAuth, AuthBase
from requests_jwt import JWTAuth


class TokenAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r


class TestDeployment(unittest.TestCase):
    def Post(self):
        url = 'http://127.0.0.1:8000/app_service/manage/me1kd/test_service'
        fn1 = os.path.join(os.path.dirname(__file__), 'test_inputs/test.sh.zip')
        fn2 = os.path.join(os.path.dirname(__file__), 'test_inputs/testconf.json')
        files = {'file': open(fn1, 'rb'),
                 'service_conf': open(fn2, 'rb')}
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.post(url, files=files, auth=auth)
        return r.status_code

    def Get(self):
        url = 'http://127.0.0.1:8000/app_service/manage/me1kd/test_service'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        request_temp = requests.get(url, auth=auth, stream=True)
        # for chunk in request_temp.iter_content(chunk_size=8192):
        #    print(chunk)
        return request_temp.status_code

    def Delete(self):
        url = 'http://127.0.0.1:8000/app_service/manage/me1kd/test_service'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.delete(url, auth=auth)
        # print(r)
        return r.status_code

    # This sequence of tests must be executed all together to ensure post is successful and the delete can remove the posted service.
    # Or the service needs to be delete seperately(this can be achieved by comment out the Post() step) to ensure the test can be passed.
    def test_deployment_workflow(self):
        self.assertEqual(self.Post(), 201)
        self.assertEqual(self.Get(), 200)
        self.assertEqual(self.Delete(), 204)


suite = unittest.TestLoader().loadTestsFromTestCase(TestDeployment)
unittest.TextTestRunner(verbosity=2).run(suite)
