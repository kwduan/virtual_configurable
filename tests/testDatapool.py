__author__ = 'Kewei Duan'

import requests, unittest, os
from requests.auth import HTTPBasicAuth, AuthBase
from requests_jwt import JWTAuth


class TokenAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r


class TestDatapool(unittest.TestCase):
    def Post(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/test'
        fn = os.path.join(os.path.dirname(__file__), 'test_inputs/abc.txt')
        files = {'file': open(fn, 'rb')}
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.post(url, files=files, auth=auth)
        # print(r)
        return r.status_code

    def Put(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/test'
        fn = os.path.join(os.path.dirname(__file__), 'test_inputs/abc.txt')
        files = {'file': open(fn, 'rb')}
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.put(url, files=files, auth=auth)
        # print(r)
        return r.status_code

    def Get(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/test'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        response_temp = requests.get(url, auth=auth, stream=True)
        # for chunk in request_temp.iter_content(chunk_size=8192):
        # print(chunk)
        return response_temp.status_code

    def Delete(self):
        url = 'http://127.0.0.1:8000/datapool/me1kd/testg/test'
        auth = TokenAuth(
            'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
        r = requests.delete(url, auth=auth)
        # print(r)
        return r.status_code

    def test_datapool_workflow(self):
        self.assertEqual(self.Post(), 202)
        self.assertEqual(self.Put(), 405)
        self.assertEqual(self.Get(), 200)
        self.assertEqual(self.Delete(), 204)


suite = unittest.TestLoader().loadTestsFromTestCase(TestDatapool)
unittest.TextTestRunner(verbosity=2).run(suite)
