__author__ = 'Kewei Duan'

import zipfile, shutil, subprocess, socket, ast, random, os
from . import models, configureSettings, serializers
import datapool


def extract_zip_to(ziplocation, destination_folder):
    with zipfile.ZipFile(ziplocation, "r") as z:
        z.extractall(destination_folder)


def has_service_permission(username, service_name, actual_user):
    if models.Service.objects.filter(createUser=username, serviceName=service_name).exists():
        obj = models.Service.objects.get(createUser=username, serviceName=service_name)
        """#Test code
        serializer = serializers.ServiceSerializer(obj)
        u_temp = serializer.data.get('workingDirectoryInputNameList')
        d_temp = unicode_to_dict(u_temp)
        """
        if obj.isPublicAccess:
            return True
        else:
            serializer = serializers.ServiceSerializer(obj)
            u_names = serializer.data.get('authorizedGroup')
            l_names = unicode_to_list(u_names)
            if str(actual_user) in l_names:
                return True
            else:
                return False
    else:
        return False


def command_generator(service_info, datagroup_name, dg_username):
    dg_username = str(dg_username)
    command_template = service_info.commandString
    input_list = unicode_to_list(service_info.inputNameList)
    output_list = unicode_to_list(service_info.outputNameList)
    local_commands_dict = configureSettings.local_commands
    isStdin = service_info.isStdin
    datapool_root = configureSettings.sandboxRoot
    command = command_template
    for input in input_list:
        command = command.replace('<' + input + '>', datapool_root + dg_username + '/' + datagroup_name + '/' + input)
    for output in output_list:
        output_path = datapool_root + dg_username + '/' + datagroup_name + '/' + output
        if os.path.isfile(output_path): #remove the possible left output files by previous service call.
            os.remove(output_path)
        command = command.replace('<' + output + '>', output_path)
    for local_command in local_commands_dict.keys():
        command = command.replace(local_command, local_commands_dict[local_command])
    command = replace_in_command_argument(service_info,datagroup_name,dg_username,command)
    if isStdin:
        command += ' < stdin'
    return command


def special_inputs_dispatcher(service_info, datagroup_name, dg_username, wk_dir):
    working_directory_input_name_dic = unicode_to_dict(service_info.workingDirectoryInputNameList)
    if working_directory_input_name_dic:
        datapool_root = configureSettings.sandboxRoot
        for wd_input in working_directory_input_name_dic.keys():
            shutil.copyfile(datapool_root + dg_username + '/' + datagroup_name + '/' + wd_input,
                            wk_dir + working_directory_input_name_dic[wd_input])
    return

"""
#Removed, because working directory is renewed entirely every time.
def special_outputs_cleaner(service_info, wk_dir):
    working_directory_output_name_dic = unicode_to_dict(service_info.workingDirectoryOutputNameList)
    if working_directory_output_name_dic:
        for wd_output in working_directory_output_name_dic.keys():
            output_path = wk_dir + working_directory_output_name_dic[wd_output]
            if os.path.isfile(output_path):
                os.remove(output_path)
    return
"""


def command_executor(command, dg_username, datagroup_name, wk_dir):
    # while models.Service.isAppLocked:
    #    continue
    # model_temp = models.Service(isAppLocked = True)
    # model_temp.save()

    datapool_root = configureSettings.sandboxRoot
    stdout = open(datapool_root + str(dg_username) + '/' + datagroup_name + '/stdout', 'wb')
    stderr = open(datapool_root + str(dg_username) + '/' + datagroup_name + '/stderr', 'wb')
    proc = subprocess.Popen(command, shell=True, stdout=stdout, stderr=stderr, cwd=wk_dir)
    proc.wait()

    # model_temp = models.Service(isAppLocked = True)
    # model_temp.save()
    return proc.returncode
    #return 0


def output_URI_generator(service_root, dg_username, datagroup_name, service_info, wk_dir):
    """ Generate the output URIs in a dictionary

    :param service_root:
    :param dg_username:
    :param datagroup_name:
    :param service_info:
    :param wk_dir:
    :return output_URIs:
    """
    output_URIs = dict()
    output_list = unicode_to_list(service_info.outputNameList)
    working_directory_output_name_dic = unicode_to_dict(service_info.workingDirectoryOutputNameList)
    for output in output_list:
        if output not in working_directory_output_name_dic.keys():
            register_in_datapool_models(dg_username, datagroup_name, output)
            output_URIs[output] = service_root + 'datapool/' + str(dg_username) + '/' + datagroup_name + '/' + output
    for wd_outupt in working_directory_output_name_dic.keys():
        shutil.copyfile(wk_dir + working_directory_output_name_dic[wd_outupt].datapool_root + str(
            dg_username) + '/' + datagroup_name + '/' + wd_outupt)
        register_in_datapool_models(dg_username, datagroup_name, wd_outupt)
        output_URIs[wd_outupt] = service_root + 'datapool/' + str(dg_username) + '/' + datagroup_name + '/' + wd_outupt
    if service_info.isStdout:
        register_in_datapool_models(dg_username, datagroup_name, 'stdout')
        output_URIs['stdout'] = service_root + 'datapool/' + str(dg_username) + '/' + datagroup_name + '/stdout'
    return output_URIs


def register_in_datapool_models(dg_username, datagroup_name, data_name):
    """ Register for the newly created output in datapool.

    :param dg_username:
    :param datagroup_name:
    :param data_name:
    :return:
    """
    if not datapool.models.DataItem.objects.filter(creatingUser=dg_username, dataGroupName=datagroup_name,
                                                   dataName=data_name).exists():
        data_item_temp = datapool.models.DataItem(creatingUser=dg_username, dataGroupName=datagroup_name,
                                                  dataName=data_name)
        data_item_temp.save()
    return


def getServiceRoot():
    return socket.gethostbyname(socket.gethostname()) + ':' + configureSettings.porNumber + '/'


def unicode_to_list(uc_str):
    """ Convert the unicode string returned by the model to python list

    :param uc_str: Unicode String
    :return l_names: List
    """
    l_names = list()
    for item in ast.literal_eval(uc_str):
        l_names.append(item.encode('ascii'))
    return l_names


def unicode_to_dict(uc_str):
    """ Convert the unicode string returned by the model to python dictionary

    :param uc_str: Univode String
    :return d_temp: Dict
    """
    d_temp = ast.literal_eval(uc_str)
    return d_temp


def working_directory_creator(service_creator, service_name):
    """ Create a temporary working directory for current service call

    :param service_creator:
    :param service_name:
    :return:
    """
    app_repo_root = configureSettings.appRepoRoot
    instance_number = random.randrange(0, 1000)
    src_path = app_repo_root + service_creator + '/' + service_name + '/'
    dest_path = app_repo_root + 'working_directories/' + service_creator + '/' + service_name + '_' + str(
        instance_number) + '/'
    shutil.copytree(src_path, dest_path)
    return dest_path


def working_directory_destroyer(dest_path):
    """ Destory the temporary working directory for current service call

    :param dest_path:
    :return:
    """
    shutil.rmtree(dest_path)
    return

def replace_in_command_argument(service_info, datagroup_name, dg_username, semi_command):
    command = semi_command
    dg_username = str(dg_username)
    input_arguments_list = unicode_to_list(service_info.inCommandArgumentInputList)
    datapool_root = configureSettings.sandboxRoot
    for input in input_arguments_list:
        input_path = datapool_root + dg_username + '/' + datagroup_name + '/' + input
        file = open(input_path,'rb')
        arguement = file.read()
        command = semi_command.replace('<' + input + '>', arguement)
    return command
