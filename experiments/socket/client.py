import socket, sys, os, time


def writeResultFile(size, item_f):
    item = "%.3f" % item_f
    file = open("result.csv", "ab")
    if(size == '1g'):
        file.write(item+'\n')
    else:
        file.write(item+',')

start_time = time.time()

file = open('./'+sys.argv[1]+'.txt',"rb")
sock = socket.socket()
sock.connect(("127.0.0.1", 10000))

while True:
    chuck = file.read(65536)
    if not chuck:
        sock.close()
        break
    sock.sendall(chuck)

while os.path.exists('test_data.txt') is False:
    continue

elapsed_time = time.time() - start_time
writeResultFile(sys.argv[1], elapsed_time)
print elapsed_time

