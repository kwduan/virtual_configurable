__author__ = 'Kewei Duan'

import requests_unixsocket, requests, json

# To run this, Docker needs to be installed as prerequiste.

local_unix_socket_path = 'http+unix://%2Fvar%2Frun%2Fdocker.sock'

def getContainerList():
    session = requests_unixsocket.Session()
    url = local_unix_socket_path+'/containers/json'
    r = session.get(url, stream=True)
    list = json.loads(r.text)
    return list

def createContainer(json_definition):
    session = requests_unixsocket.Session()
    url = local_unix_socket_path+'/containers/create'
    headers = {'Content-type': 'application/json'}
    r = session.post(url, data=json_definition, headers=headers)
    resp_info = json.loads(r.text)
    return resp_info

def startContainer(ID):
    session = requests_unixsocket.Session()
    url = local_unix_socket_path+'/containers/'+ID+'/start'
    r = session.post(url)
    return r.text

def stopContainer(ID):
    session = requests_unixsocket.Session()
    url = local_unix_socket_path+'/containers/'+ID+'/stop'
    r = session.post(url)
    return r.text

def deleteContainer(ID):
    session = requests_unixsocket.Session()
    url = local_unix_socket_path+'/containers/'+ID
    r = session.delete(url)
    return r.text

