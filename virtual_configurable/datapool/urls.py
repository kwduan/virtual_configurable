__author__ = 'Kewei Duan'

from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
                       url(r'^(?P<username>\w+)/(?P<data_group_name>\w+)/(?P<data_name>\w+)$', views.DataItemResponse.as_view()),
                       url(r'^(?P<username>\w+)/(?P<data_group_name>\w+)$', views.DataGroupResponse.as_view()),
                       url(r'^(?P<username>\w+)/(?P<data_group_name>\w+)/permission$', views.DataGroupPermissionResponse.as_view()),
                       )