__author__ = 'Kewei Duan'

from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FileUploadParser
from . import configureSettings
from . import models, utils, serializers

import os, shutil, json


class ServiceManipulation(APIView):
    parser_classes = (FileUploadParser,)

    def get(self, request, username, service_name, format=None):
        """

        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        if utils.has_service_permission(username, service_name, request.user):
            if not models.Service.objects.filter(createUser=username, serviceName=service_name).exists():
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                model = models.Service.objects.get(createUser=username, serviceName=service_name)
                serializer = serializers.ServiceSerializer(model)
                return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def post(self, request, username, service_name, format=None):
        """

        ---
        parameters:
            - name: file
              paramType: form
              type : file
              required : true
              description: The ZIP file that contains the executable and libraries.
            - name: service_conf
              paramType: form
              type: file
              required: true
              description: The configuration file in the form of JSON
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        if str(request.user) != str(username):
            return Response(status=status.HTTP_403_FORBIDDEN)
        else:
            if models.Service.objects.filter(createUser=username, serviceName=service_name).exists():
                return Response(status=status.HTTP_409_CONFLICT)
            else:
                # retrieve zip file and the configureation file
                relative_path = username + '/' + service_name + '/'
                absolute_path = configureSettings.appRepoRoot + relative_path
                zip_file = request.FILES['file']
                json_configure = request.FILES['service_conf']
                json_dict = json.load(json_configure)

                if str(json_dict['createUser']) != str(request.user):
                    return Response('Error: The createUser in the configuration file is not the logged in user',
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                if str(json_dict['createUser']) != str(username) or str(json_dict['serviceName']) != str(service_name):
                    return Response(
                        'Error: The username or service name in the configuration file does not match the URI',
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                # save the zip file
                zip_location = absolute_path + service_name + '.zip'
                if not os.path.exists(absolute_path):
                    os.makedirs(absolute_path)
                file_temp = open(zip_location, 'wb')
                for chunk in zip_file.chunks():
                    file_temp.write(chunk)
                    file_temp.flush()
                file_temp.close()

                # extract the zip file and delete it
                utils.extract_zip_to(zip_location, absolute_path)
                os.remove(zip_location)

                # create the item in service model
                serializer = serializers.ServiceSerializer(data=json_dict)
                if serializer.is_valid():
                    serializer.save()
                return Response(status=status.HTTP_201_CREATED)

    """
    def put(self, request, username, service_name, format=None):
        # To avoid user accidently update the service with new service name that may incur URI confusion.
        # If user wants do that, do it explicitly by deleting first.
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    """

    def delete(self, request, username, service_name, format=None):
        """

        ---
        parameters:
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        if str(request.user) != str(username):
            return Response(status=status.HTTP_403_FORBIDDEN)
        else:
            if not models.Service.objects.filter(createUser=username, serviceName=service_name).exists():
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                model = models.Service.objects.get(createUser=username, serviceName=service_name)
                model.delete()
                relative_path = username + '/' + service_name + '/'
                absolute_path = configureSettings.appRepoRoot + relative_path
                shutil.rmtree(absolute_path)
                return Response(status=status.HTTP_204_NO_CONTENT)


class ServiceInvocation(APIView):
    renderer_classes = (JSONRenderer,)

    def get(self, request, creator_name, service_name, format=None):
        """

        ---
        parameters:
            - name: dg_name
              paramType: query
              type : string
              required : true
              description: The name of the data group, its creator name is assumed as the logged in one.
            - name: HTTP_AUTHORIZATION
              paramType: header
              type: string
              required: true
              description: The Authorization header. It can be basic or JWT.
        """
        if utils.has_service_permission(creator_name, service_name, request.user):
            if not models.Service.objects.filter(createUser=creator_name, serviceName=service_name).exists():
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                datagroup_name = request.GET.get('dg_name', '')
                if not datagroup_name:
                    return Response("Error: The service needs the argument \"dg_name\".",
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                else:
                    # Get the root of this service instance.
                    service_root = utils.getServiceRoot()
                    # Get the metadata of the application and service.
                    service_info = models.Service.objects.get(createUser=creator_name, serviceName=service_name)
                    # Create a designated working directory for the current service call and copy an instance of the application to it.
                    wk_dir = utils.working_directory_creator(service_creator=creator_name, service_name=service_name)
                    # Move the inputs with default (hard_coded relative path) path to the working directory
                    utils.special_inputs_dispatcher(service_info=service_info, datagroup_name=datagroup_name,
                                                    dg_username=request.user, wk_dir=wk_dir)
                    # Generate the command line for this application
                    command_line = utils.command_generator(service_info, datagroup_name, dg_username=request.user)
                    if not utils.command_executor(command=command_line, dg_username=request.user,
                                                  datagroup_name=datagroup_name, wk_dir=wk_dir):  # Run the command
                        # Create the URIs for all the outputs
                        dict_output = utils.output_URI_generator(service_root=service_root, dg_username=request.user,
                                                                 datagroup_name=datagroup_name,
                                                                 service_info=service_info, wk_dir=wk_dir)
                        # Demolish the designated working directory
                        utils.working_directory_destroyer(wk_dir)
                        return Response(data=dict_output, status=status.HTTP_200_OK)

                        # return Response(status=status.HTTP_200_OK)
                    else:
                        output_URIs = dict()
                        output_URIs['stderr'] = service_root + request.user + '/' + datagroup_name + '/stderr'
                        utils.working_directory_destroyer(wk_dir)
                        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    """
    def post(self, request, format=None):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def put(self, request, format=None):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, format=None):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    """
