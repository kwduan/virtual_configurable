__author__ = 'Kewei Duan'

from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
                       url(r'^manage/(?P<username>\w+)/(?P<service_name>\w+)$', views.ServiceManipulation.as_view()),
                       url(r'^(?P<creator_name>\w+)/(?P<service_name>\w+)$', views.ServiceInvocation.as_view()),
                       )