__author__ = 'Kewei Duan'

from django.contrib import admin

from .models import DataItem, DataGroup

admin.site.register(DataItem)
admin.site.register(DataGroup)


