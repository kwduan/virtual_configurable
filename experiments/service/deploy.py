__author__ = 'me1kd'

import requests, os
from requests.auth import AuthBase


class TokenAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r


url_root = 'http://127.0.0.1:8000'
zipPath = './experiment.sh.zip'
confPath = './expconf.json'


def Deploy(zipPath, confPath):
    url = url_root + '/app_service/manage/me1kd/experiment_service'
    fn1 = os.path.join(zipPath)
    fn2 = os.path.join(confPath)
    files = {'file': open(fn1, 'rb'),
             'service_conf': open(fn2, 'rb')}
    auth = TokenAuth(
        'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
    r = requests.post(url, files=files, auth=auth)
    return r.status_code


print Deploy(zipPath, confPath)
