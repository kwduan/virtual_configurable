__author__ = 'me1kd'

import sys, requests, os, time, shutil, gc
from requests.auth import AuthBase
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
import urllib2


class TokenAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['Authorization'] = self.token
        return r


url_root = 'http://192.168.56.101:8000'

test_list = ['100m', '200m', '300m', '400m', '500m', '600m', '700m', '800m', '900m', '1g']

test_data_folder = '/home/me1kd/Develop/Projects/Test_Apps/test_data/'

"""
def Post(path):
    url = url_root + '/datapool/me1kd/testexp/input1'
    fn = os.path.join(path)
    files = {'file': open(fn, 'rb')}
    auth = TokenAuth(
        'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
    r = requests.post(url, files=files, stream=True, auth=auth)
    r.connection.close()
    print(r)
    return r
"""

def Post(path):
    url = url_root + '/datapool/me1kd/testexp/input1'
    authHeader = 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs'
    register_openers()
    datagen, headers = multipart_encode({'file': open(path, 'rb')})
    request = urllib2.Request(url, datagen, headers)
    request.add_header("Authorization", authHeader)
    print urllib2.urlopen(request).read()

def Invoke():
    url = url_root + '/app_service/me1kd/experiment_service?dg_name=testexp'
    auth = TokenAuth(
        'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
    r = requests.get(url, auth=auth)
    # for chunk in r.iter_content(chunk_size=8192):
    #    print(chunk)
    # print(r)
    return r

def Delete():
    url = url_root + '/datapool/me1kd/testexp/input1'
    auth = TokenAuth(
        'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1lMWtkIiwidXNlcl9pZCI6MSwiZW1haWwiOiJrLmR1YW5Ac2hmZmllbGQuYWMudWsiLCJleHAiOjE0MzIwNTI2NjV9.vvwP35Z9G27f8a7FCTF4XvkCZ3I4lweKm_qXiXCuEKs')
    r = requests.delete(url, auth=auth)
    # print(r)
    return r.status_code


def writeResultFile(size, item_f):
    item = "%.3f" % item_f
    file = open("result.csv", "ab")
    if (size == '1g'):
        file.write(item + '\n')
    else:
        file.write(item + ',')


def main():
    for num in range(0, 9):
        print 'start loop ' + str(num)
        for test_item in test_list:
            print 'start test' + test_item
            shutil.copyfile(test_data_folder + test_item + '.txt', './' + test_item + '.txt')
            start_time = time.time()
            Post('./' + test_item + '.txt')
            Invoke()
            elapsed_time = time.time() - start_time
            writeResultFile(test_item, elapsed_time)
            os.remove('./' + test_item + '.txt')
            Delete()
	    gc.collect()


if __name__ == "__main__":
    main()
